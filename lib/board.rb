class Board
  attr_reader :grid

  def blank_grid
    [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]]
  end

  def initialize(grid = blank_grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
    i = pos[0]
    j = pos[1]
    @grid[i][j] = mark
  end

  def empty?(pos)
    i = pos[0]
    j = pos[1]
    return true if @grid[i][j] == nil
    return false
  end

  def winner
    return :X if x_win?
    return :O if o_win?
  end

  def over?
    return false if @grid == blank_grid
    return true if x_win? || o_win?
    return true if @grid.flatten.none? { |mark| mark == nil}

  end

  def x_win?
    return true if
    @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X ||
    @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X ||
    @grid[0][0] == :X && @grid[1][0] == :X && @grid[2][0] == :X ||
    @grid[0][1] == :X && @grid[1][1] == :X && @grid[2][1] == :X ||
    @grid[0][2] == :X && @grid[1][2] == :X && @grid[2][2] == :X ||
    @grid[0][0] == :X && @grid[0][1] == :X && @grid[0][2] == :X ||
    @grid[1][0] == :X && @grid[1][1] == :X && @grid[1][2] == :X ||
    @grid[2][0] == :X && @grid[2][1] == :X && @grid[2][2] == :X
  end

  def o_win?
    return true if
    @grid[0][2] == :O && @grid[1][1] == :O && @grid[2][0] == :O ||
    @grid[0][0] == :O && @grid[0][1] == :O && @grid[0][2] == :O ||
    @grid[0][0] == :O && @grid[1][0] == :O && @grid[2][0] == :O ||
    @grid[0][1] == :O && @grid[1][1] == :O && @grid[2][1] == :O ||
    @grid[0][2] == :O && @grid[1][2] == :O && @grid[2][2] == :O ||
    @grid[0][0] == :O && @grid[0][1] == :O && @grid[0][2] == :O ||
    @grid[1][0] == :O && @grid[1][1] == :O && @grid[1][2] == :O ||
    @grid[2][0] == :O && @grid[2][1] == :O && @grid[2][2] == :O
  end

end
