require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @player_one.mark = :X
    @player_two.mark = :O
    @board = Board.new
    @current_player = player_one
  end


  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player = current_player == player_one ? player_two : player_one  
  end

end
