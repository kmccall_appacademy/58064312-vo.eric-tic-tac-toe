class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to go?"
    move = gets.chomp
    move.split(", ").map { |position| position.to_i}
  end

  def display(board)
    puts board.grid
  end
end
