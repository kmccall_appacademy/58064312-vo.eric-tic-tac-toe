class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def random_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |column|
        square = [row, column]
        moves << square
      end
    end
    return moves.sample
  end

  def get_move

    return [0,0] if board.grid[0][1] == :O && board.grid[0][2] == :O
    return [0,1] if board.grid[0][0] == :O && board.grid[0][2] == :O
    return [0,2] if board.grid[0][0] == :O && board.grid[0][1] == :O

    return [1,0] if board.grid[1][1] == :O && board.grid[1][2] == :O
    return [1,1] if board.grid[1][0] == :O && board.grid[1][2] == :O
    return [1,2] if board.grid[1][0] == :O && board.grid[1][1] == :O

    return [2,0] if board.grid[2][1] == :O && board.grid[2][2] == :O
    return [2,1] if board.grid[2][0] == :O && board.grid[2][2] == :O
    return [2,2] if board.grid[2][0] == :O && board.grid[2][1] == :O

    return [0,0] if board.grid[1][0] == :O && board.grid[2][0] == :O
    return [1,0] if board.grid[0][0] == :O && board.grid[2][0] == :O
    return [2,0] if board.grid[0][0] == :O && board.grid[1][0] == :O

    return [0,1] if board.grid[1][1] == :O && board.grid[2][1] == :O
    return [1,1] if board.grid[0][1] == :O && board.grid[2][1] == :O
    return [2,1] if board.grid[0][1] == :O && board.grid[1][1] == :O

    return [0,2] if board.grid[1][2] == :O && board.grid[2][2] == :O
    return [1,2] if board.grid[0][2] == :O && board.grid[2][2] == :O
    return [2,2] if board.grid[0][2] == :O && board.grid[1][2] == :O

    return [0,0] if board.grid[1][1] == :O && board.grid[2][2] == :O
    return [1,1] if board.grid[0][0] == :O && board.grid[2][2] == :O
    return [2,2] if board.grid[0][0] == :O && board.grid[1][1] == :O

    return [0,2] if board.grid[1][1] == :O && board.grid[2][0] == :O
    return [1,1] if board.grid[0][2] == :O && board.grid[2][0] == :O
    return [2,0] if board.grid[0][2] == :O && board.grid[1][1] == :O
    random_move
  end
end
